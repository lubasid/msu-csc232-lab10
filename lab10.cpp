/**
 * CSC232 - Data Structures with C++, Spring 2017
 * @authors Jim Daehn,
 *			Lyubov Sidlinskaya <sidlinskaya1@live.missouristate.edu>
 * @brief Lab 10: Dynamic Programming - Minimum Liability Problem
 *        Due Date: 23:59 Saturday 29 April 2017
 */

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>

// Function Prototypes - You must implement these functions.

/**
 * Display the contents of a two-dimensional integer array.
 *
 * @param table the two dimensional array whose contents is displayed in tabular
 *       format
 * @param numRows the number of rows in the two dimensional array
 * @param numColumns the number of columns in the two dimensional array
 * @post The contents of table are displayed in tabular format to the standard
 *       output device. The contents of the table are unchanged.
 */
void display(int** table, const int& numRows, const int& numColumns);

/**
 * Calculate the minimum liability amassed in traveling from the
 * upper left-hand corner of a table to cell (m, n).
 *
 * @param matrix the two dimensional array whose contents liabilities
 * @param m the destination row in the matrix
 * @param n the destination column in the matrix
 * @return The minimum liability incurred in traversing the matrix from the upper
 *         left-hand corner to the destination cell (m, n)
 * @post The contents of the matrix are unchanged.
 */
int minLiability(int** matrix, const int& m, const int& n);

/**
 * Calculate the minimum value amongst three integers.
 *
 * @param a one integer in the comparison
 * @param b another integer in the comparison
 * @param c the third integer in the comparison
 * @return The minimum of a, b, c is returned.
 * @post Netier a nor b nor c is changed.
 */
int min(const int& a, const int& b, const int& c);

/**
 * Entry point of this application.
 *
 * @param argc the number of command-line arguments
 * @param argv the command-line arguments
 */
int main(int argc, char** argv) {
	// The data file containing matrix data
	std::string inputFile;
	// Check to see if the user has supplied a specific input file for consumption
	if (argc > 1) {
		inputFile = argv[1];
	} else {
		inputFile = "data.txt";
	}

	// A stream contining input data to process
	std::ifstream dataFile(inputFile, std::ios::in);

	// Check if stream is valid
	if (!dataFile) {
		std::cout << "Could not open " << inputFile << "..." << std::endl;
		// Invalid file, exit prematurely
		exit(EXIT_FAILURE);
	} else {
		// We have a valid file that is assumed to be properly formatted
		// As long as there is data to read...
		while (!dataFile.eof()) {
			// First items to read are the number of rows and columns for the current
			// matrix
			int rows;
			int cols;

			// Get the number of rows and columns for the current matrix
			dataFile >> rows >> cols;

			// The next set of items to be read are the target row and column
			int targetRow;
			int targetCol;
			dataFile >> targetRow >> targetCol;

			// create the array needed... a dynamic, two-dimensional array is an int**
			int **matrix;
			// Create the rows
			matrix = new int*[rows];
			// Create the columns
			for (int i { 0 }; i < rows; ++i) {
				matrix[i] = new int[cols];
			}

			// read data into array
			for (int row { 0 }; row < rows; ++row) {
				for (int col { 0 }; col < cols; ++col) {
					// read data from input file
					dataFile >> matrix[row][col];
				}
			}

			// Print liablity matrix
			display(matrix, rows, cols);

			// Compute the minimum liability
			int min = minLiability(matrix, targetRow, targetCol);
			std::cout << std::endl << "Minimum liability reaching ("
					<< targetRow << ", " << targetCol << ") is " << min
					<< std::endl << std::endl;

			// we're done processing the current matrix so let's get rid of it. If more
			// data exists, a new matrix with the appropriate dimensions will be created
			// in the next iteration of this loop.
			for (int i { 0 }; i < rows; ++i) {
				delete[] matrix[i];
				matrix[i] = nullptr;
			}
			delete[] matrix;
			matrix = nullptr;
		}
	}

	// No more data; program ends
	return EXIT_SUCCESS;
}

void display(int** matrix, const int& rows, const int& cols) {
	for (int i = 0; i < rows; ++i) {
		for (int k = 0; k < cols; ++k){
			std::cout << matrix[i][k] << " ";
		}
		std::cout << std::endl;
	}
}

int minLiability(int** matrix, const int& targetRow, const int& targetCol) {
	int minArray[targetRow][targetCol];		// Creates a new Array.
	minArray[0][0] = matrix[0][0];			// Assigns new array the same location as the parameter matrix.

	//Complete First Column.
	for (int a = 1; a <= targetRow; a++) {	
		minArray[a][0] = minArray[a-1][0] + matrix[a][0];
	}
	//Complete Top Row.
	for (int b = 1; b <= targetCol; b++) {
		minArray[0][b] = minArray[0][b-1] + matrix[0][b];	
	}

	//Fill the rest of the table. 
	for (int c = 1; c <= targetRow; c++) {
		for (int d = 1; d <= targetCol; d++){
			minArray[c][d] = min(minArray[c-1][d-1], minArray[c-1][d], minArray[c][d-1]) + matrix[c][d];
		}
	}
	return minArray[targetRow][targetCol];
}

int min(const int& a, const int& b, const int& c) {
	if (a < b)
	{ 	return (a < c)? a : c; }
   	else
   	{	return (b < c)? b : c; }
}
